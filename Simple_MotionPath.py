######################################################################
#                       Simple Motion Path
#
#   Produced by : Taehyung Kim
######################################################################

import maya.cmds as cmds

def createWindow():
    window = cmds.window(title="windowUI",iconName='Short Name', widthHeight=(200, 115))
    cmds.columnLayout( adjustableColumn=True )
    cmds.button(label="Create",command = createCurve)
    cmds.button( label='+', command = pCurve)
    cmds.button( label='-', command = mCurve)
    cmds.button( label='delete', command = deleteCurve)
    cmds.button( label='Close', command=('cmds.deleteUI(\"' + window + '\", window=True)') )
    cmds.showWindow(window)

def createCurve(*args):
    curveVal = cmds.curve(name="fcurve+1",p=[(0,0,0),(0,0,5),(0,0,10),(0,0,15),(0,0,20)])
    curveShape = cmds.listRelatives(curveVal)
    spans = cmds.getAttr(curveShape[0]+".spans")
    degree = cmds.getAttr(curveShape[0]+".degree")
    numCV=spans+degree

    for i in range(0,numCV):
        curve_vtx = cmds.cluster(curveShape[0]+".cv[{0}]".format(i), relative = True)
        cmds.parent( curve_vtx, curveVal, relative = True)
    
    cmds.select(curveVal,r=True)   
    
def pCurve(*args):
    cmds.delete(ch=True)
    cmds.makeIdentity(a=True,t=1,r=1,s=1,n=0)
    list = cmds.ls(sl=True)
    cmds.select(list[0])
    s_curve = list[0]
    curveShape = cmds.listRelatives(s_curve)    
    spans = cmds.getAttr(curveShape[0]+".spans")
    degree = cmds.getAttr(curveShape[0]+".degree")
    cmds.rebuildCurve(curveShape,spans=spans+1)
    numCV=(spans+1)+degree

    for i in range(0,numCV):
        curve_vtx = cmds.cluster(curveShape[0]+".cv[{0}]".format(i), relative = True)
        cmds.parent( curve_vtx, s_curve, relative = True)
    cmds.select(s_curve,r=True)

def mCurve(*args):
    cmds.delete(ch=True)
    cmds.makeIdentity(a=True,t=1,r=1,s=1,n=0)
    list = cmds.ls(sl=True)
    cmds.select(list[0])
    s_curve = list[0]
    curveShape = cmds.listRelatives(s_curve)    
    spans = cmds.getAttr(curveShape[0]+".spans")
    degree = cmds.getAttr(curveShape[0]+".degree")
    cmds.rebuildCurve(curveShape,spans=spans-1)
    numCV=(spans+1)+degree
    for i in range(0,numCV):
        curve_vtx = cmds.cluster(curveShape[0]+".cv[{0}]".format(i), relative = True)
        cmds.parent( curve_vtx, s_curve, relative = True)  
    cmds.select(s_curve,r=True)
    
def deleteCurve(*args):
    list = cmds.ls(sl=True)
    cmds.delete(list)
    
createWindow()
#createCurve() 